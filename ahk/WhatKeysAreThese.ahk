#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

^+F7::
MsgBox, 16, Remuter, Control Shift F7 DISABLE CAMERA, 3
return

^+F8::
MsgBox, 32, Remuter, Control Shift F8 ENABLE CAMERA, 3
return

^+F9::
MsgBox, 16, Remuter, Control Shift F9 MUTE MICROPHONE, 3
return

^+F10::
MsgBox, 32, Remuter, Control Shift F10 UNMUTE MICROPHONE, 3
return

^+F11::
MsgBox, 16, Remuter, Control Shift F11 MUTE SPEAKERS, 3
return

^+F12::
MsgBox, 32, Remuter, Control Shift F12 UNMUTE SPEAKERS, 3
return
