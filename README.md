# RemuterMCS

An easy to build Bluetooth remote for controlling microphone/camera/speakers in your video conference sessions.

This is a work in progress right now, but you can expect me to flesh things out when it gets closer to completion. To read more about the project as it progresses, you can follow along at https://hackaday.io/project/177896-remutermcs

You can see a video demo here: https://youtu.be/5W1djk3L4SI
