#!/bin/bash

# Given an SVG file, generate a series of square XBM header files in the sizes we need.
# The SVG is assumed to be bounded by just the actual graphics. The conversion shrinks the
# graphics inside the square a bit (92%).

if [ "x$1" -eq "x" ]
then
    echo "Pass the basename of the SVG file as an argument. For example, for 'foo.svg', pass 'foo'."
    exit 1
fi

B="$1"
S=${B}.svg

for extent in 40 56 80 128
do
    resize=$(((92*$extent)/100))
    echo convert -gravity center ${S} -resize ${resize}x${resize} -extent ${extent}x${extent} ${B}${extent}.xbm
         convert -gravity center ${S} -resize ${resize}x${resize} -extent ${extent}x${extent} ${B}${extent}.xbm
         sed -i -e "1i#pragma once" -e "s/static char/static const unsigned char/g" ${B}${extent}.xbm
	 mv ${B}${extent}.xbm ${B}${extent}.xbm.h
done
