/**
 * This sketch makes it easy to experiment with color combinations
 * for the iconography. You get to pick the colors. The sketch
 * slowly cycles through screen showing the microphone, camera,
 * and speaker icons for all 3 states and in 80 and 40 pixel versions.
 * They are also shown in pairs for positive and negative space.
 * The last few frames simulate changing focus among the 3 targets.
 *
 * Color choices are the same as described in RemuterMCSconfig.h,
 * either pre-defined color names or custom RGB colors using the
 * color565(r,g,b) construct. The colors you see here "out of the
 * box" are the same as the defaults for RemuteMCS.
 *
 * Obviously, this sketch is not intended to be comprehensive or
 * elegant. It's just a quick way to try things.
 * To run this sketch, copy the *.xbm.h files included below from
 * the icons/ directory into the same directory as this sketch.
 * (Copying them is a limitation of the Arduino IDE. Otherwise,
 * you could just reference them by a relative path to the icons/
 * directory.)
 */

// We assume we are running on a M5StickC (not M5StickC Plus) with
// a 160h x 80w display. Icons are stacked vertically.

#include <M5StickC.h>

#define color565(r,g,b) (((r & 0xF8) << 8) | ((g & 0xFC) << 3) | (b >> 3))

#define DELAY 2000
#define COLOR_FOR_BACKGROUND BLACK
#define COLOR_FOR_ENABLED    BLUE
#define COLOR_FOR_DISABLED   color565(255, 69, 0)
#define COLOR_FOR_NEUTRAL    DARKGREY

#include "mic80.xbm.h"
#include "spk80.xbm.h"
#include "cam80.xbm.h"

#include "mic40.xbm.h"
#include "spk40.xbm.h"
#include "cam40.xbm.h"

void setup() {
  M5.begin();
  M5.Lcd.setRotation(2);
}

void loop(){

  // 80 pixel microphone
  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   mic80_bits, 80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawXBitmap(0,80,  mic80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   mic80_bits, 80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_NEUTRAL);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawXBitmap(0,80,  mic80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   mic80_bits, 80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (78,0,               0,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (79,0,               0,79, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,0,               0,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,1,               1,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,2,               2,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawXBitmap(0,80,  mic80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (78,80,             0,158, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (79,80,             0,159, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,80,             0,160, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,81,             1,160, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,82,             2,160, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  // 40 pixel microphone
  M5.Lcd.fillRect   (0,0,               40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   mic40_bits, 40,40, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,40,              40,40, COLOR_FOR_ENABLED);
  M5.Lcd.drawXBitmap(0,40,  mic40_bits, 40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.fillRect   (0,80,              40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,80,  mic40_bits, 40,40, COLOR_FOR_NEUTRAL);
  M5.Lcd.fillRect   (0,120,             40,40, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawXBitmap(0,120, mic40_bits, 40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.fillRect   (40,0,              40,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(40,0,  mic40_bits, 40,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (40,80,             40,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (40,120,            40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(40,80, mic40_bits, 40,40, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  // 80 pixel camera
  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   cam80_bits, 80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawXBitmap(0,80,  cam80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   cam80_bits, 80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_NEUTRAL);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawXBitmap(0,80,  cam80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);

  delay(DELAY);
  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   cam80_bits, 80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (78,0,               0,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (79,0,               0,79, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,0,               0,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,1,               1,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,2,               2,80, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawXBitmap(0,80,  cam80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (78,80,             0,158, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (79,80,             0,159, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,80,             0,160, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,81,             1,160, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,82,             2,160, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  // 40 pixel camera
  M5.Lcd.fillRect   (0,0,               40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   cam40_bits, 40,40, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,40,              40,40, COLOR_FOR_ENABLED);
  M5.Lcd.drawXBitmap(0,40,  cam40_bits, 40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.fillRect   (0,80,              40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,80,  cam40_bits, 40,40, COLOR_FOR_NEUTRAL);
  M5.Lcd.fillRect   (0,120,             40,40, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawXBitmap(0,120, cam40_bits, 40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.fillRect   (40,0,              40,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(40,0,  cam40_bits, 40,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (40,80,             40,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (40,120,            40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(40,80, cam40_bits, 40,40, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  // 80 pixel speakers
  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   spk80_bits, 80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawXBitmap(0,80,  spk80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   spk80_bits, 80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_NEUTRAL);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawXBitmap(0,80,  spk80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   spk80_bits, 80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (78,0,               0,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (79,0,               0,79, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,0,               0,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,1,               1,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,2,               2,80, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawXBitmap(0,80,  spk80_bits, 80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (78,80,             0,158, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (79,80,             0,159, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,80,             0,160, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,81,             1,160, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawLine   (80,82,             2,160, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  // 40 pixel speakers
  M5.Lcd.fillRect   (0,0,               40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   spk40_bits, 40,40, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,40,              40,40, COLOR_FOR_ENABLED);
  M5.Lcd.drawXBitmap(0,40,  spk40_bits, 40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.fillRect   (0,80,              40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,80,  spk40_bits, 40,40, COLOR_FOR_NEUTRAL);
  M5.Lcd.fillRect   (0,120,             40,40, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawXBitmap(0,120, spk40_bits, 40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.fillRect   (40,0,              40,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(40,0,  spk40_bits, 40,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (40,80,             40,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (40,120,            40,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(40,80, spk40_bits, 40,40, COLOR_FOR_BACKGROUND);
  delay(DELAY);

  // microphone has focus
  M5.Lcd.fillRect   (0,0,               80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,0,   mic80_bits, 80,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawRect   (1,1,               78,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (78,0,               0,78, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (79,0,               0,79, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,0,               0,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,1,               1,80, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (80,2,               2,80, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (0,80,              80,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(20,80, cam40_bits, 40,40, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,120,             80,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(20,120,spk40_bits, 40,40, COLOR_FOR_NEUTRAL);
  delay(DELAY);

  // camera has focus
  M5.Lcd.fillRect   (0,0,               80,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(20,0,  mic40_bits, 40,40, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (59,0,              20,39, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (60,0,              20,40, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (60,1,              21,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (0,40,              80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,40,  cam80_bits, 80,80, COLOR_FOR_ENABLED);
  M5.Lcd.drawRect   (1,41,              78,78, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,120,             80,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(20,120,spk40_bits, 40,40, COLOR_FOR_NEUTRAL);
  delay(DELAY);

  // speakers have focus
  M5.Lcd.fillRect   (0,0,               80,40, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(20,0,  mic40_bits, 40,40, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (59,0,              20,39, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (60,0,              20,40, COLOR_FOR_DISABLED);
  M5.Lcd.drawLine   (60,1,              21,40, COLOR_FOR_DISABLED);
  M5.Lcd.fillRect   (0,40,              80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(20,40, cam40_bits, 40,40, COLOR_FOR_ENABLED);
  M5.Lcd.fillRect   (0,80,              80,80, COLOR_FOR_BACKGROUND);
  M5.Lcd.drawXBitmap(0,80,  spk80_bits, 80,80, COLOR_FOR_NEUTRAL);
  M5.Lcd.drawRect   (1,79,              78,78, COLOR_FOR_NEUTRAL);
  delay(DELAY);

}
