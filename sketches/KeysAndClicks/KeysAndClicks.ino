/**
 * Reacts to single and double clicks, but the reaction just prints
 * what should happen.
 */

#include <AceButton.h>
#include <M5StickC.h>

using namespace ace_button;

const int LED_PIN = M5_LED;
const int LED_OFF = HIGH;
const int LED_ON  = LOW;

// The pin number attached to the button.
#define ACTION_BUTTON_PIN M5_BUTTON_HOME
#define MODE_BUTTON_PIN   M5_BUTTON_RST

AceButton actionButton(ACTION_BUTTON_PIN, HIGH, 1);
AceButton modeButton(MODE_BUTTON_PIN, HIGH, 2);
AceButton *buttons[] = {&actionButton, &modeButton};
const uint8_t NUMBER_OF_BUTTONS = sizeof(buttons) / sizeof(buttons[0]);

void setup() {
  Serial.begin(115200);
  Serial.println("Starting");

  pinMode(LED_PIN, OUTPUT);
  turnLedOff();

  pinMode(ACTION_BUTTON_PIN, INPUT_PULLUP);
  pinMode(MODE_BUTTON_PIN,   INPUT_PULLUP);

  // using method 3, ClickVersusDoubleClickUsingBoth, to distinguish clicks
  ButtonConfig* actionButtonConfig = actionButton.getButtonConfig();
  actionButtonConfig->setEventHandler(handleButtonEvent);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureDoubleClick);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureSuppressClickBeforeDoubleClick);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureSuppressAfterClick);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureSuppressAfterDoubleClick);

  ButtonConfig* modeButtonConfig = modeButton.getButtonConfig();
  modeButtonConfig->setEventHandler(handleButtonEvent);
  modeButtonConfig->setFeature(ButtonConfig::kFeatureDoubleClick);
  modeButtonConfig->setFeature(ButtonConfig::kFeatureSuppressClickBeforeDoubleClick);
  modeButtonConfig->setFeature(ButtonConfig::kFeatureSuppressAfterClick);
  modeButtonConfig->setFeature(ButtonConfig::kFeatureSuppressAfterDoubleClick);
}

typedef struct targetDevice {
  int id;
  const char *name;
} targetDevice_t;

targetDevice_t microphone = {0, "microphone"};
targetDevice_t camera     = {1, "camera"};
targetDevice_t speaker    = {2, "speaker"};
targetDevice_t *targetDevices[] = {&microphone, &camera, &speaker};
const uint8_t NUMBER_OF_TARGET_DEVICES = sizeof(targetDevices) / sizeof(targetDevices[0]);
int activeTargetDeviceNumber = 0;

#define ACTION_CLICK_NONE   0
#define ACTION_CLICK_SINGLE 1
#define ACTION_CLICK_DOUBLE 2
int actionClickType = ACTION_CLICK_NONE;

void loop() {
  manageLedState();

  actionButton.check();
  if (actionClickType != ACTION_CLICK_NONE) {
    Serial.print("action click "); Serial.println(actionClickType == 1 ? "SINGLE" : "DOUBLE");
    actionClickType = ACTION_CLICK_NONE;
  }

  int previousActiveTargetDeviceNumber = activeTargetDeviceNumber;
  modeButton.check();
  if (activeTargetDeviceNumber != previousActiveTargetDeviceNumber) {
    Serial.print("change focus to "); Serial.println(targetDevices[activeTargetDeviceNumber]->name);
  }

}

void manageLedState() {
  // We forego the debouncing of the button library in order to be able
  // to accurately control the LED for press and release. If we get the
  // button states via AceButton, the LED reaction is slowed down by the
  // AceButton built-in delays.
  bool anyButtonIsPressed = false;
  for (int ii=0; ii<NUMBER_OF_BUTTONS; ++ii) {
    int pin = buttons[ii]->getPin();
    if (digitalRead(pin) == LOW) {
      anyButtonIsPressed = true;
      break;
    }
  }
  if (anyButtonIsPressed) {
    turnLedOn();
  } else {
    turnLedOff();
  }
}

bool ledIsOn = true; // for boot-up purposes
void turnLedOn() {
  if (!ledIsOn) {
    digitalWrite(LED_PIN, LED_ON);
  }
  ledIsOn = true;
}

void turnLedOff() {
  if (ledIsOn) {
    digitalWrite(LED_PIN, LED_OFF);
  }
  ledIsOn = false;
}

void handleButtonEvent(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  if (button->getPin() == ACTION_BUTTON_PIN) {
    handleActionButtonEvent(button, eventType, buttonState);
  } else if (button->getPin() == MODE_BUTTON_PIN) {
    handleModeButtonEvent(button, eventType, buttonState);
  } else {
    // shouldn't happen
    Serial.print("Unknown button pin "); Serial.println(button->getPin(), DEC);
  }
}

void handleActionButtonEvent(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  actionClickType = ACTION_CLICK_NONE;
  switch (eventType) {
    case AceButton::kEventClicked:
    case AceButton::kEventReleased:
      actionClickType = ACTION_CLICK_SINGLE;
      break;
    case AceButton::kEventDoubleClicked:
      actionClickType = ACTION_CLICK_DOUBLE;
      break;
  }
}

void handleModeButtonEvent(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  switch (eventType) {
    case AceButton::kEventClicked:
    case AceButton::kEventReleased:
      activeTargetDeviceNumber++;
      break;
    case AceButton::kEventDoubleClicked:
      // just for demonstrating double click of the mode button
      activeTargetDeviceNumber += 2;
      break;
  }
  activeTargetDeviceNumber %= NUMBER_OF_TARGET_DEVICES;
}
