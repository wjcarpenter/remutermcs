/*
 * Blinker.h
 */
#pragma once
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

class Blinker {
public:
  Blinker();
  void blinkIfYouLoveDisabled();
  void turnLedOn();
  void turnLedOff();
};

extern Blinker rLED;
