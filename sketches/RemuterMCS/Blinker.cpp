/*
 * Blinker.cpp
 */
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

#ifdef M5STICKC_PLUS
#include <M5StickCPlus.h>
#else
#include <M5StickC.h>
#endif


static const uint8_t LedPinNumber = M5_LED;
static const uint8_t LedOffLevel  = HIGH;
static const uint8_t LedOnLevel   = LOW;

static bool ledIsOn = true;
static bool blinkIsLit = false;
static unsigned long blinkOn  = 0;
static unsigned long blinkOff = 0;

Blinker::Blinker() {
  pinMode(LedPinNumber, OUTPUT);
  turnLedOff();
}

void Blinker::blinkIfYouLoveDisabled() {
  bool anyTargetDisabledAndBlinks = rTARGETSET.isAnyTargetDisabled(true);
  if (!anyTargetDisabledAndBlinks) {
    blinkIsLit = false;
    turnLedOff();
    return;
  }
  unsigned long now = millis();
  if (ledIsOn) {
    if ((now - blinkOn) > BLINK_LED_WHILE_DISABLED_DURATION_MILLIS) {
      blinkIsLit = false;
      turnLedOff();
      blinkOff = now;
    }
  } else {
    if ((now - blinkOff) > BLINK_LED_WHILE_DISABLED_PERIOD_MILLIS) {
      blinkIsLit = true;
      turnLedOn();
      blinkOn = now;
    }
  }
}

void Blinker::turnLedOn() {
  if (!ledIsOn) {
    digitalWrite(LedPinNumber, LedOnLevel);
  }
  ledIsOn = true;
}

void Blinker::turnLedOff() {
  if (blinkIsLit) return;
  if (ledIsOn) {
    digitalWrite(LedPinNumber, LedOffLevel);
  }
  ledIsOn = false;
}

Blinker rLED;
