/*
 * Buttons.cpp
 */
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"
#include <AceButton.h>
using namespace ace_button;

#ifdef M5STICKC_PLUS
#include <M5StickCPlus.h>
#else
#include <M5StickC.h>
#endif

// The pin number attached to the button.
static const uint8_t ActionButtonPin = M5_BUTTON_HOME;
static const uint8_t ModeButtonPin   = M5_BUTTON_RST;
static AceButton actionButton(ActionButtonPin);
static AceButton modeButton(ModeButtonPin);
static AceButton *buttons[] = {&actionButton, &modeButton};
static const uint8_t NumberOfButtons = sizeof(buttons) / sizeof(buttons[0]);

bool Buttons::isAnyButtonPressed() {
  // We forego the debouncing of the button library in order to be able
  // to accurately control the LED for press and release. If we get the
  // button states via AceButton, the LED reaction is slowed down by the
  // AceButton built-in delays.
  for (int ii=0; ii<NumberOfButtons; ++ii) {
    int pin = buttons[ii]->getPin();
    if (digitalRead(pin) == LOW) {
      return true;
    }
  }
  return false;
}

/**
 * The "action" button is the big flat button on the same surface as the display.
 * It has "M5" embossed on it. It acts as the enable (double click) and disable
 * (single click) button for the target that currently has focus. The action is
 * performed whether or not the display is currently on.
 */
static void handleActionButtonEvent(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  ActionClick actionClickType = None;
  switch (eventType) {
    case AceButton::kEventPressed:
      rDISPLAY.smudge();
      break;
    case AceButton::kEventClicked:
    case AceButton::kEventReleased:
      actionClickType = Single;
      rDISPLAY.smudge();
      break;
    case AceButton::kEventDoubleClicked:
      actionClickType = Double;
      rDISPLAY.smudge();
      break;
  }
  setActionClickType(actionClickType);
}

static void modeButtonCommon() {
  if (rDISPLAY.isOn()) {
    rTARGETSET.bumpFocus();
  }
  rDISPLAY.smudge();
}

/**
 * The "mode" button is used to select the target (microphone, camera, speakers) by
 * cycling through them. It does not act if the display is not on, so a single click
 * merely turns the display on. That makes it a handy way to check the state of the
 * targets if the display is off.
 *
 * The "mode" button is either of the buttons on the side of the M5SticC. Due to the
 * inherent electronics of the M5StickC, those button presses have to be detected
 * in different ways. The button closer to the big "M5" button is also the power
 * button. It's not wired to an ESP32 GPIO and is checked by accessing the AXP192
 * power controller in the function powerButtonCheck(). The button further from the
 * big "M5 button is a general purpose button with checking via a callback from the
 * button library.
 *
 * Regardless of which button is pressed, the same behavior results, and it's factored
 * out into modeButtonCommon()
 */
static void handleModeButtonEvent(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  switch (eventType) {
    case AceButton::kEventPressed:
      modeButtonCommon();
      break;
  }
}

static void handleButtonEvent(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  if (button->getPin() == ActionButtonPin) {
    handleActionButtonEvent(button, eventType, buttonState);
  } else if (button->getPin() == ModeButtonPin) {
    handleModeButtonEvent(button, eventType, buttonState);
  } else {
    // shouldn't happen
    Serial.printf("Unknown button pin %d\b", button->getPin());
  }
}

Buttons::Buttons() {
  pinMode(ActionButtonPin, INPUT_PULLUP);
  pinMode(ModeButtonPin,   INPUT_PULLUP);
  // using method 3, ClickVersusDoubleClickUsingBoth, to distinguish clicks
  ButtonConfig* actionButtonConfig = actionButton.getButtonConfig();
  actionButtonConfig->setEventHandler(handleButtonEvent);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureDoubleClick);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureSuppressClickBeforeDoubleClick);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureSuppressAfterClick);
  actionButtonConfig->setFeature(ButtonConfig::kFeatureSuppressAfterDoubleClick);

  // only detecting press (not release or click)
  ButtonConfig* modeButtonConfig = modeButton.getButtonConfig();
  modeButtonConfig->setEventHandler(handleButtonEvent);
}

void Buttons::checkActionButton() {
  actionButton.check();
}
void Buttons::checkPowerButton() {
  if (M5.Axp.GetBtnPress() != 0) {
      modeButtonCommon();
  }
}

void Buttons::checkModeButton() {
  modeButton.check();
}

Buttons rBUTTONS;
