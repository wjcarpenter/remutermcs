/*
 * TargetSet.cpp
 */

#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

Target::Target(TargetType type, TargetState state, const KeyPressSequence *disableKPS, const KeyPressSequence *enableKPS, boolean blinkLedWhileDisabled)
    : type(type), state(state), disableKPS(disableKPS), enableKPS(enableKPS), blinkLedWhileDisabled(blinkLedWhileDisabled) {}

TargetType Target::getType() { return type; }
TargetState Target::getState() { return state; }
void Target::setState(TargetState _state) { state = _state; }
const KeyPressSequence *Target::getDisableKPS() { return disableKPS; }
const KeyPressSequence *Target::getEnableKPS() { return enableKPS; }
bool Target::isBlinkLedWhileDisabled() {return blinkLedWhileDisabled; }

TargetSet::TargetSet() {
  // I wanted to do within an initializer, but C++ outlasted me in that.
  // Do these need to be static? Not sure.
  static Target _m {
    Microphone, INITIAL_STATE_MICROPHONE,
    KPS_DISABLE_MICROPHONE, KPS_ENABLE_MICROPHONE,
    BLINK_LED_WHILE_MICROPHONE_DISABLED
  };
  static Target _c {
    Camera, INITIAL_STATE_CAMERA,
    KPS_DISABLE_CAMERA, KPS_ENABLE_CAMERA,
    BLINK_LED_WHILE_CAMERA_DISABLED
  };
  static Target _s {
    Speakers, INITIAL_STATE_SPEAKERS,
    KPS_DISABLE_SPEAKERS, KPS_ENABLE_SPEAKERS,
    BLINK_LED_WHILE_SPEAKERS_DISABLED
  };
  static Target *_mcs[] {&_m, &_c, &_s};
  numberOfTargets = sizeof(_mcs) / sizeof(_mcs[0]);
  targets = _mcs;
  targetWithFocus = 0;
  for (int ii; ii<numberOfTargets; ++ii) {
    Target *t = targets[ii];
    if (t->getType() == INITIAL_FOCUS) {
      targetWithFocus = ii;
      break;
    }
  }
}

Target *TargetSet::getTargetWithFocus() {
  return targets[targetWithFocus];
}

void TargetSet::bumpFocus() {
  targetWithFocus++;
  targetWithFocus %= numberOfTargets;
}

/*
 * If isBlinker is true, we only care about disabled targets
 * that are set to blink.
 */
bool TargetSet::isAnyTargetDisabled(bool isBlinker) {
  bool anyTargetDisabled = false;
  for (int ii=0; ii<numberOfTargets; ++ii) {
    Target *t = targets[ii];
    if (t->getState() == Disabled) {
      if (!isBlinker  ||  t->isBlinkLedWhileDisabled()) {
        return true;
      }
    }
  }
  return false;
}


uint8_t TargetSet::getNumberOfTargets() { return numberOfTargets; }
Target *TargetSet::getTarget(uint8_t n) { return targets[n]; }

TargetSet rTARGETSET;
