/*
 * Display.h
 */
#pragma once
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

class Display {
public:
  Display();
  unsigned long setExpirationRelative(unsigned long relativeMillis);
  void manageState(WhatToDisplay whatToDisplay);
  void smudge();
  bool isOn();
  void setDisplayBrightness(int brightness);
};

extern Display rDISPLAY;
