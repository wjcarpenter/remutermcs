#pragma once
#include <Arduino.h>
#include <BleKeyboard.h>
#include <BLEDevice.h>
using KeyPressSequence = const uint8_t;

enum M5Orientation {
  M5ButtonTop,
  M5ButtonBottom,
  M5ButtonLeft,
  M5ButtonRight,  
};


enum WhatToDisplay {
  Splash,
  Targets
};

enum ActionClick {
  None,
  Single,
  Double
};

// I borrowed this macro definition from the method TFT_eSPI::color565(r,g,b)
#define color_565(r,g,b) (((r & 0xF8) << 8) | ((g & 0xFC) << 3) | (b >> 3))

extern void setActionClickType(ActionClick type);

#include "Display.h"
#include "Blueteeth.h"
#include "Blinker.h"
#include "TargetSet.h"
#include "Power.h"
#include "Buttons.h"
