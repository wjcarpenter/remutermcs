// display brightness here: https://github.com/geiseri/esphome-m5stickC/blob/master/components/axp192/axp192.cpp
// https://community.home-assistant.io/t/do-you-have-an-m5stick-c-working-with-current-esphome/210895/28?u=wjcarpenter

// #define SKIPBT 1

// RemuterMCS.h is typical C++ stuff to separate struct, typedefs, etc. You should not change it.
#include "RemuterMCS.h"

// RemuterMCSconfig.h is for customizations of behaviors if you don't like my choices.
#include "RemuterMCSconfig.h"
#include <string>

#ifdef M5STICKC_PLUS
#include <M5StickCPlus.h>
#else
#include <M5StickC.h>
#endif
#include <Preferences.h>

#define NAP_DURATION 20
static const char *key_naptime      = "naptime";
static const char *key_longevity    = "longevity";
static const char *key_batteryVlow  = "batteryVlow";
static const char *key_batteryVhigh = "batteryVhigh";
#define PREFERENCES_SAVE_EVERY_MILLIS 600000
static unsigned long storedLongevity = 0;
static unsigned long storedNaptime = 0;
static unsigned long lastSaveMillis = 0;

static ActionClick actionClickType = None;
void setActionClickType(ActionClick type) {
  actionClickType = type;
}
Preferences preferences;


void setup() {
  setCpuFrequencyMhz(80);
  // M5.begin writes to serial console at 115200
  M5.begin(true, true, false); // LCD, Power, Serial
  rDISPLAY.setDisplayBrightness(DISPLAY_BRIGHTNESS);
  Serial.begin(500000);
  
  Serial.println();
  Serial.printf("RemuterMCS Starting as %s\n", rBT.getDeviceName().c_str());
  Serial.printf("BT device %s\n", rBT.getMacAddress().c_str());

  preferences.begin("RemuterMCS");
  storedLongevity    = preferences.getULong64(key_longevity);
  storedNaptime      = preferences.getULong64(key_naptime);
  float storedBatteryVLow  = preferences.getFloat(key_batteryVlow,  3.0);
  float storedBatteryVHigh = preferences.getFloat(key_batteryVhigh, 4.2);
  preferences.end();
  rPOWER.setBatteryStoredRange(storedBatteryVLow, storedBatteryVHigh);
  Serial.printf("Stored longevity = %ld (minutes = %ld), naptime = %ld\n", storedLongevity, storedLongevity/(1000*60), storedNaptime);
  Serial.printf("Stored battery low voltage = %.4fv, high voltage = %.4fv\n", storedBatteryVLow, storedBatteryVHigh);

  rBT.connect();
  rDISPLAY.setExpirationRelative(DISPLAY_TIMEOUT_MILLIS);
  rDISPLAY.smudge();
 }

static void maybeTakeANap() {
  if (NAP_DURATION > 0  &&  (!rDISPLAY.isOn() || rPOWER.isPluggedIn())) {
    delay(NAP_DURATION);
  }
}

/**
 * This function figures out what key press sequence to send and makes a
 * call to send it.
 */
static void performActionClick() {
  if (actionClickType != None) {
    Target *target = rTARGETSET.getTargetWithFocus();
    Serial.printf("action click %s\n", actionClickType == Single ? "single DISABLE " : "double ENABLE ");
    const KeyPressSequence *kpToSend;
    if (actionClickType == Single) {
      target->setState(Disabled);
      kpToSend = target->getDisableKPS();
    } else {
      target->setState(Enabled);
      kpToSend = target->getEnableKPS();
    }
    rBT.sendKeys(kpToSend);
    actionClickType = None;
  }
}

static void performModeClick(bool changeFocus) {
  if (changeFocus) {
    //Serial.printf("mode change focus to %s\n", targets[targetWithFocus]->name);
  }
}

static void manageLedStateForButtons() {
  if (!BUTTON_PRESS_LIGHTS_LED) return;
  bool anyButtonIsPressed = rBUTTONS.isAnyButtonPressed();
  // No exposed API to read power button without resetting it
  // so we forego lighting the LED for that case.
  if (anyButtonIsPressed) {
    rLED.turnLedOn();
  } else {
    rLED.turnLedOff();
  }
}

static void maybeSavePreferences() {
  long now = millis();
  if ((now - lastSaveMillis) > PREFERENCES_SAVE_EVERY_MILLIS) {
    lastSaveMillis = now;
    preferences.begin("RemuterMCS");
    preferences.putULong64(key_longevity, now);
    preferences.putULong64(key_naptime, NAP_DURATION);
    float voltage = M5.Axp.GetBatVoltage();
    float storedBatteryVLow;
    float storedBatteryVHigh;
    rPOWER.getBatteryStoredRange(&storedBatteryVLow, &storedBatteryVHigh);
    Serial.printf("Stored longevity = %ld (minutes = %ld), naptime = %ld\n", storedLongevity, storedLongevity/(1000*60), storedNaptime);
    Serial.printf("Stored battery low voltage = %.4fv, high voltage = %.4fv\n", storedBatteryVLow, storedBatteryVHigh);
    Serial.printf("Measured battery %fv compared to stored low %fv, stored high %fv\n", voltage, storedBatteryVLow, storedBatteryVHigh);
    if (voltage < storedBatteryVLow) {
      preferences.putFloat(key_batteryVlow, voltage);
    }
    if (rPOWER.isPluggedIn()  &&  !rPOWER.isCharging()) {
      // The AXP192 has a target charging voltage of 4.2v, but
      // if the voltage is already within 5% of that, it won't
      // turn on charging. Only update the voltage high water mark
      // if it's more than 5% different from 4.2 or if we haven't
      // yet stored anything (we defaulted to 0.0).
      float difference = abs(voltage - 4.2);
      float diffFraction = difference / 4.2;
      Serial.printf("Plugged in, not charging, difference from 4.2v is %.2f%%\n", diffFraction*100);
      if (storedBatteryVHigh < 1.0  ||  diffFraction > 0.05) {
        preferences.putFloat(key_batteryVhigh, voltage);
      }
    }
    preferences.end();
  }
}

void loop() {
  maybeTakeANap();
  maybeSavePreferences();
  rLED.blinkIfYouLoveDisabled();

  // When is the right time to call these manage* functions? It's
  // complicated, but really doesn't matter much for the short
  // LONGEVITY of these loops.
  manageLedStateForButtons();

  if (rDISPLAY.isOn()) {
    if (rPOWER.didPowerStatusChange()) rDISPLAY.smudge();
    if (rPOWER.didBatteryLevelChange()) rDISPLAY.smudge();
    if (rBT.isRssiChanged()) rDISPLAY.smudge();
  }

  rBUTTONS.checkActionButton();
  performActionClick();
  rDISPLAY.manageState(Targets);

  Target *previousTargetWithFocus = rTARGETSET.getTargetWithFocus();
  // treat these two as the same thing; easy to get confused
  rBUTTONS.checkPowerButton();
  rBUTTONS.checkModeButton();
  performModeClick(previousTargetWithFocus != rTARGETSET.getTargetWithFocus());
  rDISPLAY.manageState(Targets);

}

