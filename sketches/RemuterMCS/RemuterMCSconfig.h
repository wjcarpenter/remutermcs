#pragma once

// If you have an M5StickC Plus, uncomment the following #define. If you
// have an M5StickC (the earlier non-plus version) leave it commented out.

//#define M5STICKC_PLUS

////////////////////////////////////////////////////////////////
// Colors. There are color choices involved. You get to choose them all!
// You can use any of these pre-defined colors:
//
// BLACK, NAVY, DARKGREEN, DARKCYAN, MAROON, PURPLE, OLIVE,
// LIGHTGREY, DARKGREY, BLUE, GREEN, CYAN, RED, MAGENTA, YELLOW
// WHITE, ORANGE, GREENYELLOW, PINK
//
// Or, you can define a custom color directly as an unsigned 16-bit RGB
// value in 5-6-5 format. You can use the convenience function
// color_565(r,g,b) ... for example, color_565(220, 20, 60) for crimson.
// All three values must be between 0 and 255, inclusive.
// There are lots of places to find RGB color charts. Just one of
// many is here: https://gitlab.com/wjcarpenter/esphome-colors
// Colors on tiny TFT displays don't always come out the way you
// expect, so you may have to experiment.
//

// This is traditionally GREEN, but I'm using BLUE as the default in case
// you have red-green deficiency.
#define COLOR_FOR_ENABLED BLUE

// This is traditionally RED, but I'm using an ORANGE-ish as the default in case
// you have red-green deficiency.
#define COLOR_FOR_DISABLED color_565(255, 69, 0)

// This is used to indicate "unknown" and a few other subdued things.
#define COLOR_FOR_NEUTRAL DARKGREY

// Good choices for this are BLACK and WHITE. You want something that
// helps the other colors be very visible.
#define COLOR_FOR_BACKGROUND BLACK

// When drawing icons, do you want to reverse the colors for the
// background versus the color representing the icon state? If so,
// set this to true.
#define FLIP_ICON_COLOR false

// When a target is disabled, we draw a slash through its icon in the
// defined color. You can instead opt to have the slash drawn in the
// same color as the icon itself (including honoring the FLIP_ICON_COLOR
// setting). If you want to use your own color choice instead of the
// icon color, set the second line below to false. The color you set
// will be used regardless of the setting of FLIP_ICON_COLOR.
#define COLOR_FOR_DISABLED_SLASH LIGHTGREY
#define USE_ICON_COLOR_FOR_DISABLED_SLASH_COLOR false

// When a target has focus, we draw a box around its icon in the
// defined color. You can instead opt to have the box drawn in the
// same color as the icon itself (including honoring the FLIP_ICON_COLOR
// setting). If you want to use your own color choice instead of the
// icon color, set the second line below to false. The color you set
// will be used regardless of the setting of FLIP_ICON_COLOR.
#define COLOR_FOR_FOCUS_BOX WHITE
#define USE_ICON_COLOR_FOR_FOCUS_BOX_COLOR true

////////////////////////////////////////////////////////////////
/*
 * A KeyPressSequence is something that can be sent in response to an
 * Action key single or double click. It's an array of unsigned 8 bit
 * values which make up a list of lists. A number of keys can be
 * pressed simultaneously, and they are identified as a list of keys:
 * there is a number identifying how many keys, then that many
 * keys. That can be followed by another number and list of keys for
 * as many lists as you'd like. Finally, a 0 value terminates the
 * overall definition. This sounds confusing, but it should be clearer
 * if you look at the default values in the definitions just below.
 *
 * Each item in a single list causes a keypress API call (as if you
 * pressed that key with your finger and held it down). After they
 * have all been pressed, there is a releaseAll API call (as if you
 * had taken your hands away from all keys).
 *
 * Keys to be pressed can be either defined constants from
 * "BleKeyboard.h" or the equivalent numeric value (but why would
 * you?). For printable ASCII characters, a literal character value
 * like 'e' can be used (the API converts it to the correct scan
 * code). To use a shifted printable character, you can either supply
 * that value literally or you can supply KEY_LEFT_SHIFT or
 * KEY_RIGHT_SHIFT and the unshifted character (but why would you?).
 *
 * The only reason you would change any of these is because they conflict with
 * something tied to that key sequence on the PC side.
 */
static const KeyPressSequence KPS_DISABLE_MICROPHONE[] {3, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_F9,  0}; //Control-Shift-F9;
static const KeyPressSequence KPS_ENABLE_MICROPHONE[]  {3, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_F10, 0}; //Control-Shift-F10;
static const KeyPressSequence KPS_DISABLE_CAMERA[]     {3, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_F7,  0}; //Control-Shift-F7;
static const KeyPressSequence KPS_ENABLE_CAMERA[]      {3, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_F8,  0}; //Control-Shift-F8;
static const KeyPressSequence KPS_DISABLE_SPEAKERS[]   {3, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_F11, 0}; //Control-Shift-F11;
static const KeyPressSequence KPS_ENABLE_SPEAKERS[]    {3, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_F12, 0}; //Control-Shift-F12;
////////////////////////////////////////////////////////////////

// M5Orientation is you prefer to see things displayed on the M5StickC.
// The choices are   M5ButtonTop, M5ButtonBottom, M5ButtonLeft,and M5ButtonRight.
// That refers to where the big "M5" button is when you are looking at
// the device.
//
// There are two different kinds of display screens:
//
// * The splash screen, which you see on start-up while the device
//   is making a Bluetooth connection. Text is horizontal, so the
//   only reasonable choices are M5ButtonLeft and M5ButtonRight.
// * The targets screen displaying the icons for microphone, camera,
//   and speaker. The icons are stacked vertically, so the only
//   reasonable choices are M5ButtonTop and M5ButtonBottom.
//
// If you make an unreasonable choice :-) for either screen, you'll get
// my default choices (M5ButtonLeft and M5ButtonTop, respectively).

static const M5Orientation SPLASH_ORIENTATION  = M5ButtonLeft;
static const M5Orientation TARGETS_ORIENTATION = M5ButtonTop;

////////////////////////////////////////////////////////////////
// When you first turn the M5StickC on, it doesn't know if the
// various target devices are muted or unmuted, so it considers
// them to be in an unknown state until you act on them. If you
// always start your sessions with the same setup, you can
// designate an initial state for each target device. If you
// get it wrong in any particular session, you can get the displayed
// icons back in sync by just doing a mute or unmute action, as
// appropriate.
//
// Possible values for these initial states are Unknown, Enabled,
// and Disabled (case sensitive, no quotes).
#define INITIAL_STATE_MICROPHONE Unknown
#define INITIAL_STATE_CAMERA     Unknown
#define INITIAL_STATE_SPEAKERS   Enabled

// When you first turn on the M5StickC, one of the devices will
// have "focus", meaning that the Action button will act on that
// target device. It's also displayed distinctively. You can
// choose which target device has that initial focus.
//
// Possible values for initial focus are Microphone, Camera, and
// Speakers (case sensitive, no quotes).
#define INITIAL_FOCUS Microphone

////////////////////////////////////////////////////////////////

// LED can blink periodically when one or more of the targets is disabled. This
// happens even if the display is off. It's a kind of reminder that you are
// muted, dummy. :-)
//
// You get to decide which targets making the blinking happen when disabled,
// and you can also change the blink rate. The first group should be set
// to the boolean values true or false. The second set is an positive integer
// number of milliseconds.

// LED periodicaly blinks while microphone is disabled
#define BLINK_LED_WHILE_MICROPHONE_DISABLED true
// LED periodicaly blinks while camera is disabled
#define BLINK_LED_WHILE_CAMERA_DISABLED     false
// LED periodicaly blinks while speakers are disabled
#define BLINK_LED_WHILE_SPEAKERS_DISABLED   false

// this often; default 1.5 seconds
#define BLINK_LED_WHILE_DISABLED_PERIOD_MILLIS    1500
// ON for this long; default 10 milliseconds
#define BLINK_LED_WHILE_DISABLED_DURATION_MILLIS    10

// This one is slightly different. Should the LED stay lit
// while any of the buttons is pressed (and go off when
// all the buttons have been released)? The flashing LED is
// good feedback on the timing of your button presses,
// especially in distinguishing single from double clicks.
// But it can also be slightly annoying.
#define BUTTON_PRESS_LIGHTS_LED  true

////////////////////////////////////////////////////////////////

// Display timeout. After button presses and a few other actions, the
// display stays on. After this amount of time, it turns itself off
// if nothing interesting has happened. This saves some battery drain.
// default 6 seconds
#define  DISPLAY_TIMEOUT_MILLIS 6000

// If you want the display to stay on while plugged in, set this to
// true, else set it to false. If the display isn't already on when
// you plug the device in, you may have to press a button to turn
// the display on. Power or mode buttons are safest.
#define DISPLAY_ALWAYS_ON_WHILE_PLUGGED   true

// If you want the display to stay on even while on battery power,
// set this to true, else leave it set to false.
#define DISPLAY_ALWAYS_ON_WHILE_UNPLUGGED false
////////////////////////////////////////////////////////////////

// The device uses a lot of power when trying to connect over
// Blueooth. If the other end is not powered up or otherwise
// has some problem connecting, you can quickly drain the device
// battery. This config item controls how long the device will
// keep trying to connect when it is not plugged in. That is,
// when it's on battery power. When the device is plugged in, it
// will keep trying forever.
//
// When this time (in seconds) expires, the device shuts down.
#define UNPLUGGED_BLUETOOTH_CONNECTION_TIMEOUT_SECS 180
////////////////////////////////////////////////////////////////

// Screen brightness. The M5StickC and M5StickC-Plus both
// theoretically have 16 levels of brightness. However, the M5 API
// will reduce any value over 12 to just 12 (probably because of
// display voltage tolerances. Also, any value below 8 will be barely
// visible or even invisible. So, you probably want a comfortable
// value between 8 and 12, inclusive.
//
// Although it probably extends battery life a bit to use a
// lower value, it's probably a small difference in normal use
// since we switch the display off after a few seconds of
// inactivity.
#define DISPLAY_BRIGHTNESS 10
////////////////////////////////////////////////////////////////

// You can have a bar along the right long edge for battery remaining
// and you can have a bar along the left long edge for RSSI (signal
// strength. In the lower right corner (at the base of the battery bar)
// you can have a little square (called the power blotch) indicating
// the battery charging status. You can turn any or all of them off.
//
#define DRAW_BATTERY_BAR  true
#define DRAW_RSSI_BAR     true
#define DRAW_POWER_BLOTCH true
////////////////////////////////////////////////////////////////

// Typing speed. When RemuterMCS sends keystokes to the remote
// device, it types faster than any human could possibly type.
// Some remote devices can't cope with that speed, so we artificially
// slow things down. If you are not having any problem with missed
// key sequences, you don't need to change these.
//
// This delay (in milliseconds) happens after keys have been
// pressed and before the keys are released.
#define KEYPRESS_DELAY_BEFORE_RELEASE 25
// This delay (in milliseconds) happens after keys have been
// released and before the next set of presses commences.
#define KEYPRESS_DELAY_AFTER_RELEASE  25
