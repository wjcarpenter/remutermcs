/*
 * Blueteeth.h
 */
#pragma once
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

class Blueteeth {
public:
  Blueteeth();
  void connect();
  bool isConnected();
  void sendKeys(const KeyPressSequence *kps);
  void setBatteryLevel(uint8_t batteryLevel);
  bool isRssiChanged();
  float getRssiFraction();
  std::string getMacAddress();
  std::string getDeviceName();
};

extern Blueteeth rBT;
