/*
 * Power.cpp
 */
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

#ifdef M5STICKC_PLUS
#include <M5StickCPlus.h>
#else
#include <M5StickC.h>
#endif
#include <Preferences.h>

static float recentBatteryFraction = -1.0;
#define BATTERY_CHECK_EVERY_MILLIS     30000
#define POWER_CHECK_EVERY_MILLIS        5000
static unsigned long lastBatteryCheckMillis = 0;
static unsigned long lastPowerCheckMillis = 0;
static bool recentPluggedInStatus = false;
static bool recentChargingStatus  = false;
static float storedBatteryVLow = 3.0;
static float storedBatteryVHigh = 4.2;

Power::Power() {}

float Power::getRecentBatteryFraction() {
  return recentBatteryFraction;
}

void Power::setBatteryStoredRange(float low, float high) {
  storedBatteryVHigh = high;
  storedBatteryVLow = low;
}

void Power::getBatteryStoredRange(float *low, float *high) {
  *low = storedBatteryVLow;
  *high = storedBatteryVHigh;
}

void Power::getBatteryFraction() {
  float voltageRange = storedBatteryVHigh - storedBatteryVLow;
  float voltageRead = M5.Axp.GetBatVoltage();
  float voltageBounded = max(storedBatteryVLow, min(storedBatteryVHigh, voltageRead));
  float voltageFraction = (voltageBounded - storedBatteryVLow) / voltageRange;
  float vF = (voltageRead - 3.0) / 1.2;
  float vF42 = 100.0 * voltageRead / 4.2;
  Serial.printf("Battery read=%.3f, stored (%.3f, %.3f), FRACTION: %.3f, %.3f, %.2f%%, ch? %s pl? %s\n",
		voltageRead, storedBatteryVLow, storedBatteryVHigh, voltageFraction, vF, vF42,
		isCharging()?"Y":"N", isPluggedIn()?"Y":"N");
  recentBatteryFraction = voltageFraction;
}

static void doCheckBattery() {
  rPOWER.getBatteryFraction();
}

bool Power::didPowerStatusChange() {
  long now = millis();
  if ((now - lastPowerCheckMillis) > POWER_CHECK_EVERY_MILLIS) {
    lastPowerCheckMillis = now;
    bool pP = recentPluggedInStatus;
    bool nP = isPluggedIn();
    if (pP != nP) {
      recentPluggedInStatus = nP;
      return true;
    }
    bool pC = recentChargingStatus;
    bool nC = isCharging();
    if (pC != nC) {
      recentChargingStatus = nC;
      return true;
    }
  }
  return false;
}

bool Power::didBatteryLevelChange() {
  long now = millis();
  if (recentBatteryFraction < 0.0  ||  (now - lastBatteryCheckMillis) > BATTERY_CHECK_EVERY_MILLIS) {
    lastBatteryCheckMillis = now;
    float previousBatteryFraction = recentBatteryFraction;
    doCheckBattery();
    if (recentBatteryFraction != previousBatteryFraction) {
      int8_t batteryLevel = recentBatteryFraction * 100;
#ifndef SKIPBT
      rBT.setBatteryLevel(batteryLevel);
#endif
      return true;
    }
  }
  return false;
}
// AXP192 battery charge target voltage 4.2V +/- 5%
// Comments in M5 code:
//     Low Volt Level 1, when APS Volt Output < 3.4496 V
//     Low Volt Level 2, when APS Volt Output < 3.3992 V, then this flag is SET (0x01)
//     Flag will reset once battery volt is charged above Low Volt Level 1
//     Note: now AXP192 have the Shutdown Voltage of 3.0V (B100) Def in REG 31H

/*
From the AXP192 datasheet via Google translate:

REG 01H: Power supply working mode and charging status indication
Bit description
7 Indicate whether AXP192 is over temperature
  0: not over temperature;
  1: over temperature
6 Charging instructions
  0: not charging or charging completed;
  1: charging
5 Battery presence status indicator
  0: No battery is connected to AXP192;
  1: Battery is already connected to AXP192
4 Reserved and unchangeable
3 Indicates whether the battery is in active mode
  0: did not enter the battery activation mode;
  1: entered the battery activation mode
2 Indicate whether the charging current is less than the expected current
  0: The actual charging current is equal to the expected current;
  1: The actual charging current is less than the expected current
1 AXP192 switch mode indication
  0: Method A;
  1: Method B
0 Reserved and unchangeable
*/
bool Power::isCharging() {
#ifdef M5STICKC_PLUS
  uint8_t batteryChargingStatus = M5.Axp.Read8bit(0x01);
#else
  uint8_t batteryChargingStatus = M5.Axp.GetBatteryChargingStatus();
#endif
  bool isCharging = batteryChargingStatus & 0x40;
  //Serial.printf("battery charging status 0x%2X, is charging? %s  %fv\n", batteryChargingStatus, isCharging?"Y":"N", M5.Axp.GetBatVoltage());
  return isCharging;
}


/*
From the AXP192 datasheet via Google translate:

The register status bits and meanings of external power supply are shown in the following table:

Status bit of the register meaning
Register REG00H[7] Indicates whether the external adapter power ACIN is present
Register REG00H[6] Indicates whether the external adapter power ACIN is available
Register REG00H[5] Indicate whether the external power supply VBUS exists
Register REG00H[4] Indicates whether the external power supply VBUS is available
Register REG00H[3] Indicate whether the voltage of VBUS is higher than V HOLD when the external power supply VBUS is connected
Register REG00H[2] (not given in the datasheet)
Register REG00H[1] Indicate whether the external power supply ACIN/VBUS is short-circuited on the PCB
Register REG00H[0] Indicate whether the system is activated by ACIN/VBUS

"When the external power supply VBUS is connected, whether the voltage
of VBUS is higher than V HOLD" is the flag bit, which allows the Host
to receive IRQ7 (Referring to the weak power supply capability of
VBUS), judge whether VBUS is pulled down due to system load access or
because the voltage of the external power supply itself is lower thanV
HOLD , so that it is convenient for the Host software to decide
whether to continue working in the voltage limit mode or change to the
pass-through mode
*/

bool Power::isPluggedIn() {
#ifdef M5STICKC_PLUS
  uint8_t batteryPluggedStatus = M5.Axp.Read8bit(0x00);
#else
  uint8_t batteryPluggedStatus = M5.Axp.GetInputPowerStatus();
#endif
  bool isPlugged = batteryPluggedStatus & 0x50; // either USB or AC power
  //Serial.printf("battery plugged status 0x%2X, is plugged? %s  %fma\n", batteryPluggedStatus, isPlugged?"Y":"N", M5.Axp.GetBatCurrent());
  return isPlugged;
}

Power rPOWER;
