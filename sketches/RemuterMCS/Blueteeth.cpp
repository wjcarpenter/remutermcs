/*
 * RmcsBT.cpp
 */
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

static void gattsCallbackHandler(esp_gatts_cb_event_t event, esp_gatt_if_t iftype, esp_ble_gatts_cb_param_t* param);
static void gapCallbackHandler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t* param);
static std::string readMacAddress();
static void readRssi();

Blueteeth::Blueteeth() {
  BLEDevice::setCustomGapHandler(gapCallbackHandler);
  BLEDevice::setCustomGattsHandler(gattsCallbackHandler);
}

static bool btConnected = false;
static std::string btMacAddress = readMacAddress();
static std::string btDeviceName = std::string("RemuterMCS ") + btMacAddress.substr(12);
static BleKeyboard bleKeyboard(btDeviceName);
#define RSSI_CHECK_EVERY_MILLIS       900000
static esp_bd_addr_t remoteBda;
static bool remoteBdaValid = false;
static std::string remoteAddress;
static int recentRssi = 127;
static unsigned long lastRssiCheckMillis = 0;

bool Blueteeth::isConnected() {
  return btConnected;
}

void Blueteeth::connect() {
#ifdef SKIPBT
  return;
#else
  if (bleKeyboard.isConnected()) return;
  bleKeyboard.begin();
  while (!bleKeyboard.isConnected()) {
    rPOWER.didBatteryLevelChange();
    rDISPLAY.setExpirationRelative(20);
    rDISPLAY.smudge();
    rDISPLAY.manageState(Splash);
    delay(750);
  }
  btConnected = true;
  rDISPLAY.setExpirationRelative(2);
  rDISPLAY.smudge();
  rDISPLAY.manageState(Splash);
  Serial.println("BT connected");
  delay(2000);
#endif
}

void Blueteeth::setBatteryLevel(uint8_t batteryLevel) {
  bleKeyboard.setBatteryLevel(batteryLevel);
}

bool Blueteeth::isRssiChanged() {
#ifndef SKIPBT
  unsigned long now = millis();
  if ((now - lastRssiCheckMillis) > RSSI_CHECK_EVERY_MILLIS) {
    int previousRssi = recentRssi;
    readRssi();
    if (recentRssi != previousRssi) {
      return true;
    }
  }
#endif
  return false;
}

void Blueteeth::sendKeys(const KeyPressSequence *kps) {
#ifndef SKIPBT
  while (*kps != 0) {
    uint8_t count = *kps;
    //Serial.printf("------ Count %d\n", count);
    ++kps;
    for (int ii=0; ii<count; ++ii) {
      uint8_t key = *kps;
      ++kps;
      //Serial.printf("  Press %2x\n", key);
      bleKeyboard.press(key);
    }
    if (KEYPRESS_DELAY_BEFORE_RELEASE > 0) {
      delay(KEYPRESS_DELAY_BEFORE_RELEASE);
    }
    bleKeyboard.releaseAll();
    if (KEYPRESS_DELAY_AFTER_RELEASE > 0) {
      delay(KEYPRESS_DELAY_AFTER_RELEASE);
    }
  }
#endif
}

// #include <Wire.h>

// uint8_t Read8bit( uint8_t Addr )
// {
//     Wire1.beginTransmission(0x34);
//     Wire1.write(Addr);
//     Wire1.endTransmission();
//     Wire1.requestFrom(0x34, 1);
//     return Wire1.read();
// }

// static void readShutdownVoltage() {
//   uint8_t reg = Read8bit(0x31);
//   uint8_t shutterBits = reg & 0x07;
//   float voltage = 2.6 + (0.1 * shutterBits);
//   Serial.printf("Shutdown setting. reg=0x%02X, bits=0x%02X, voltage=%.1f\n", reg, shutterBits, voltage);

//   uint8_t l1 = Read8bit(0x3A);
//   uint8_t l2 = Read8bit(0x3B);

//   // this conversion is from the AXP192 datasheet
//   float fl1 = 2.8672 + ((1.4*l1*4.0)/1000.0);
//   float fl2 = 2.8672 + ((1.4*l2*4.0)/1000.0);
// }


std::string Blueteeth::getMacAddress() {
  return btMacAddress;
}

std::string Blueteeth::getDeviceName() {
  return btDeviceName;
}

static std::string readMacAddress() {
  esp_bd_addr_t mac;
  esp_read_mac(mac, ESP_MAC_BT);
  BLEAddress fromMac(mac);
  return fromMac.toString();
}

static void readRssi() {
  unsigned long now = millis();
  bool stillWaitingForFirstRssiReading = (recentRssi == 127);
  bool waitedLongEnoughSinceLastRssiCheck = ((now - lastRssiCheckMillis) > 5000);
  if (remoteBdaValid  &&  (stillWaitingForFirstRssiReading || waitedLongEnoughSinceLastRssiCheck)) {
    if (esp_ble_gap_read_rssi(remoteBda) != ESP_OK) {
      Serial.printf("Call to esp_ble_gap_read_rssi(%s) failed\n", remoteAddress.c_str());
    } else {
      lastRssiCheckMillis = now;
    }
  }
}

//static const char *GATTS_EVENT_DESCS[] = {  // from esp_gatts_defs.h
//  "REG register application id",
//  "READ client request read operation",
//  "WRITE client request write operation",
//  "EXEC_WRITE gatt client request execute write",
//  "MTU set mtu done",
//  "CONF receive confirm",
//  "UNREG unregister application id",
//  "CREATE create service done",
//  "ADD_INCL_SRVC add included service done",
//  "ADD_CHAR add characteristic done",
//  "ADD_CHAR_DESCR add descriptor done",
//  "DELETE delete service done",
//  "START start service done",
//  "STOP stop service done",
//  "CONNECT client connect",
//  "DISCONNECT client disconnect",
//  "OPEN connect to peer",
//  "CANCEL_OPEN disconnect from peer",
//  "CLOSE server close",
//  "LISTEN listen to be connected",
//  "CONGEST congestion",
//  "RESPONSE send response done",
//  "CREAT_ATTR_TAB create table done",
//  "SET_ATTR_VAL set attr value done",
//  "SEND_SERVICE_CHANGE send service change indication done"
//};

float Blueteeth::getRssiFraction() {
  // esp32 docs say the RSSI value is between -127 to 20, but I'm not
  // so sure it can actually be positive; I'm going to use the range
  // -127 to 0. 127 is a signal value that the RSSI could not be read
  // so treat that as unknown.
  if (recentRssi > 0) {
    return -1.0;
  }
  float fraction = 1.0 - (recentRssi / (-127.0));
  return fraction;
}

static void setRemoteBda(esp_bd_addr_t bda) {
  memcpy(remoteBda, bda, ESP_BD_ADDR_LEN);
  remoteAddress = BLEAddress(remoteBda).toString();
  remoteBdaValid = true;
}

static void gattsCallbackHandler(esp_gatts_cb_event_t event, esp_gatt_if_t iftype, esp_ble_gatts_cb_param_t* param) {
  //Serial.printf("BLE GATTS event %2d %s\n", event, GATTS_EVENT_DESCS[event]);
  switch (event) {
  case ESP_GATTS_CONNECT_EVT: {
    setRemoteBda(param->connect.remote_bda);
    const char *remote = remoteAddress.c_str();
    readRssi();
    Serial.printf("  BT CONN connection id: %d, remote: %s\n", param->connect.conn_id, remote);
    break;
  }
  case ESP_GATTS_DISCONNECT_EVT: {
    setRemoteBda(param->disconnect.remote_bda);
    const char *remote = remoteAddress.c_str();
    Serial.printf("  BT DISC connection id: %d, remote: %s\n", param->disconnect.conn_id, remote);
    break;
  }
  case ESP_GATTS_READ_EVT: {
    setRemoteBda(param->read.bda);
    const char *remote = remoteAddress.c_str();
    readRssi();
    Serial.printf("  BT READ connection id: %d, remote: %s\n", param->write.conn_id, remote);
    break;
  }
  case ESP_GATTS_WRITE_EVT: {
    setRemoteBda(param->write.bda);
    const char *remote = remoteAddress.c_str();
    readRssi();
    Serial.printf("  BT WRITE connection id: %d, remote: %s\n", param->write.conn_id, remote);
    break;
  }
  case ESP_GATTS_CONF_EVT: {
    readRssi();
    break;
  }
  }
}

static const char *GAP_EVENT_DESCS[] = {  // from esp_gap_ble_api.h
  "ADV_DATA_SET_COMPLETE advertising data set done",
  "SCAN_RSP_DATA_SET_COMPLETE scan response data set done",
  "SCAN_PARAM_SET_COMPLETE scan parameters set done",
  "SCAN_RESULT one scan result ready",
  "ADV_DATA_RAW_SET_COMPLETE raw advertising data set done",
  "SCAN_RSP_DATA_RAW_SET_COMPLETE raw advertising data set done",
  "ADV_START_COMPLETE start advertising done",
  "SCAN_START_COMPLETE start scan done",
  "AUTH_CMPL Authentication complete",
  "KEY key event for peer device keys",
  "SEC_REQ security request",
  "PASSKEY_NOTIF passkey notification",
  "PASSKEY_REQ passkey request",
  "OOB_REQ OOB request",
  "LOCAL_IR local IR",
  "LOCAL_ER local ER",
  "NC_REQ Numeric Comparison request",
  "ADV_STOP_COMPLETE stop adv done",
  "SCAN_STOP_COMPLETE stop scan done",
  "SET_STATIC_RAND_ADDR set the static rand address done",
  "UPDATE_CONN_PARAMS update connection parameters done",
  "SET_PKT_LENGTH_COMPLETE set pkt length done",
  "SET_LOCAL_PRIVACY_COMPLETE Enable/disable privacy on the local device done",
  "REMOVE_BOND_DEV_COMPLETE remove the bond device done",
  "CLEAR_BOND_DEV_COMPLETE clear the bond device clear done",
  "GET_BOND_DEV_COMPLETE get the bond device list done",
  "READ_RSSI_COMPLETE read the rssi done",
  "UPDATE_WHITELIST_COMPLETE add or remove whitelist done",
  "UPDATE_DUPLICATE_EXCEPTIONAL_LIST_COMPLETE update duplicate exceptional list done"
};



static void gapCallbackHandler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t* param) {
  // struct ble_read_rssi_cmpl_evt_param {
  //     esp_bt_status_t status;                     /*!< Indicate the read adv tx power operation success status */
  //     int8_t rssi;                                /*!< The ble remote device rssi value, the range is from -127 to 20, the unit is dbm,
  //                                                      if the RSSI cannot be read, the RSSI metric shall be set to 127. */
  //     esp_bd_addr_t remote_addr;                  /*!< The remote device address */
  // } read_rssi_cmpl;                               /*!< Event parameter of ESP_GAP_BLE_READ_RSSI_COMPLETE_EVT */
  Serial.printf("BLE GAP   event %2d %s\n", event, GAP_EVENT_DESCS[event]);
  if (event == ESP_GAP_BLE_READ_RSSI_COMPLETE_EVT) {
    //    esp_bd_addr_t *thatBda = &param->read_rssi_cmpl.remote_addr;
    //esp_bd_addr_t thisBda;
    //memcpy(thisBda, thatBda, ESP_BD_ADDR_LEN);
    const char *remote = BLEAddress(param->read_rssi_cmpl.remote_addr).toString().c_str();
    esp_bt_status_t status = param->read_rssi_cmpl.status;
    int8_t rssi = param->read_rssi_cmpl.rssi;
    Serial.printf("  RSSI status: %d, value was: %d now: %d, remote: %s (%s)\n", status, recentRssi, rssi, remote, remoteAddress.c_str());
    if (status == ESP_BT_STATUS_SUCCESS) {
      recentRssi = rssi;
    }
  }
}

Blueteeth rBT;
