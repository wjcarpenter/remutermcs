/*
 * Power.h
 */
#pragma once
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

class Power {
public:
  Power();
  float getRecentBatteryFraction();
  void getBatteryFraction();
  bool isCharging();
  bool isPluggedIn();
  void setBatteryStoredRange(float low, float high);
  void getBatteryStoredRange(float *low, float *high);
  bool didBatteryLevelChange();
  bool didPowerStatusChange();
};

extern Power rPOWER;
