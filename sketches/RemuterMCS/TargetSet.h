/*
 * TargetSet.h
 */
#pragma once
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

enum TargetState {
  Unknown,
  Enabled,
  Disabled
};

enum TargetType {
  Microphone,
  Camera,
  Speakers
};

class Target {
public:
  Target(TargetType type, TargetState state, const KeyPressSequence *disableKPS, const KeyPressSequence *enableKPS, boolean blinkLedWhileDisabled);
  TargetType getType();
  TargetState getState();
  void setState(TargetState state);
  const KeyPressSequence *getDisableKPS();
  const KeyPressSequence *getEnableKPS();
  bool isBlinkLedWhileDisabled();
private:
  TargetType type;
  TargetState state;
  const KeyPressSequence *disableKPS;
  const KeyPressSequence *enableKPS;
  boolean blinkLedWhileDisabled;
};

class TargetSet {
public:
  TargetSet();
  uint8_t getNumberOfTargets();
  Target *getTarget(uint8_t n);
  Target *getTargetWithFocus();
  void bumpFocus();
  bool isAnyTargetDisabled(bool isBlinker);
private:
  Target **targets;
  uint8_t numberOfTargets;
  uint8_t targetWithFocus = 0;
};

extern TargetSet rTARGETSET;
