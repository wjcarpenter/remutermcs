/*
 * Buttons.h
 */
#pragma once
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

class Buttons {
public:
  Buttons();
  void checkActionButton();
  void checkPowerButton();
  void checkModeButton();
  bool isAnyButtonPressed();
};

extern Buttons rBUTTONS;
