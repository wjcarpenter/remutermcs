/*
 * RmcsDisplay.cpp
 */
#include <string>
#include "RemuterMCS.h"
#include "RemuterMCSconfig.h"

#ifdef M5STICKC_PLUS
#include <M5StickCPlus.h>
#else
#include <M5StickC.h>
#endif


// This block abstracts the dimension differences between M5StickC and Plus for the display
#ifdef M5STICKC_PLUS
# define DISPLAY_LONG_DIMENSION  240
# define DISPLAY_SHORT_DIMENSION 135
# define SMALL_ICON_SIZE   56
# define LARGE_ICON_SIZE  128
# include "mic56.xbm.h"
# include "mic128.xbm.h"
# define smallMicIcon mic56_bits
# define largeMicIcon mic128_bits
# include "cam56.xbm.h"
# include "cam128.xbm.h"
# define smallCamIcon cam56_bits
# define largeCamIcon cam128_bits
# include "spk56.xbm.h"
# include "spk128.xbm.h"
# define smallSpkIcon spk56_bits
# define largeSpkIcon spk128_bits
#else
# define DISPLAY_LONG_DIMENSION  160
# define DISPLAY_SHORT_DIMENSION  80
# define SMALL_ICON_SIZE   40
# define LARGE_ICON_SIZE   80
# include "mic40.xbm.h"
# include "mic80.xbm.h"
# define smallMicIcon mic40_bits
# define largeMicIcon mic80_bits
# include "cam40.xbm.h"
# include "cam80.xbm.h"
# define smallCamIcon cam40_bits
# define largeCamIcon cam80_bits
# include "spk40.xbm.h"
# include "spk80.xbm.h"
# define smallSpkIcon spk40_bits
# define largeSpkIcon spk80_bits
#endif
#define SMALL_ICON_PAD_2X (DISPLAY_SHORT_DIMENSION - SMALL_ICON_SIZE)
#define LARGE_ICON_PAD_2X (DISPLAY_SHORT_DIMENSION - LARGE_ICON_SIZE)
#define SMALL_ICON_PAD_2Y (0)
#define LARGE_ICON_PAD_2Y (DISPLAY_LONG_DIMENSION - (LARGE_ICON_SIZE + 2*SMALL_ICON_SIZE))


static std::map<TargetType, const unsigned char *> largeIcons = {
		{Microphone, largeMicIcon},
		{Camera, largeCamIcon},
		{Speakers, largeSpkIcon}
};
static std::map<TargetType, const unsigned char *> smallIcons = {
		{Microphone, smallMicIcon},
		{Camera, smallCamIcon},
		{Speakers, smallSpkIcon}
};

Display::Display() {}

unsigned long expiration = millis() + DISPLAY_TIMEOUT_MILLIS;
bool isCurrentlyOn = false;
bool needsRefresh = true;

unsigned long Display::setExpirationRelative(unsigned long relativeMillis) {
  expiration = millis() + relativeMillis;
  return expiration;
}

void Display::setDisplayBrightness(int brightness) {
  M5.Axp.ScreenBreath(brightness);
}

bool Display::isOn() {
  return isCurrentlyOn;
}

static uint16_t COLORS_FOR_COLORFUL_BAR[] = {COLOR_FOR_ENABLED, COLOR_FOR_DISABLED}; //, COLOR_FOR_BACKGROUND, COLOR_FOR_NEUTRAL};
static int COLOR_COUNT_FOR_BAR = sizeof(COLORS_FOR_COLORFUL_BAR) / sizeof(COLORS_FOR_COLORFUL_BAR[0]);
static int BAR_COLOR_BLOCK_WIDTH = 2;
static int BAR_COLOR_BLOCK_HEIGHT = 8;
static void drawBar(float fraction, bool drawOnRight, bool useNeutralColor) {
  if (TARGETS_ORIENTATION == M5ButtonBottom) {
    M5.Lcd.setRotation(0);
  } else {
    M5.Lcd.setRotation(2);
  }
  fraction = max(0.0, min(fraction, 1.0));
  int barLength = DISPLAY_LONG_DIMENSION * fraction;
  int colorTick = 0;
  for (int yy=0;  yy<barLength; ++yy) {
    int pixelY = DISPLAY_LONG_DIMENSION - yy - 1;
    int pixelColor = (useNeutralColor ? COLOR_FOR_NEUTRAL :  COLORS_FOR_COLORFUL_BAR[colorTick % COLOR_COUNT_FOR_BAR]);
    for (int xx=0; xx<BAR_COLOR_BLOCK_WIDTH; ++xx) {
      int pixelX;
      if (drawOnRight) {
        pixelX = DISPLAY_SHORT_DIMENSION - xx - 1;
      } else {
        pixelX = xx;
      }
      M5.Lcd.drawPixel(pixelX, pixelY, pixelColor);
    }
    if (yy%BAR_COLOR_BLOCK_HEIGHT == (BAR_COLOR_BLOCK_HEIGHT - 1)) {
      ++colorTick;
    }
  }
}

#ifdef M5STICKC_PLUS
static int POWER_BLOTCH_SIZE   = 10;
static int POWER_BLOTCH_RADIUS =  3;
#else
static int POWER_BLOTCH_SIZE   =  7;
static int POWER_BLOTCH_RADIUS =  3;
#endif


static void drawPowerBlotch() {
  if (DRAW_POWER_BLOTCH) {
    // lower right corner; "disabled" means charging, "enabled" means fully charged
    // display charging status, but only if plugged in
    bool isPlugged  = rPOWER.isPluggedIn();
    if (isPlugged) {
      bool isCharging = rPOWER.isCharging();
      int squareColor = isCharging ? COLOR_FOR_DISABLED : COLOR_FOR_ENABLED;
      M5.Lcd.fillRoundRect(DISPLAY_SHORT_DIMENSION - POWER_BLOTCH_SIZE - 1, DISPLAY_LONG_DIMENSION - POWER_BLOTCH_SIZE - 1,
          POWER_BLOTCH_SIZE, POWER_BLOTCH_SIZE, POWER_BLOTCH_RADIUS, squareColor);
    }
  }
}

static void drawBatteryBar() {
  if (DRAW_BATTERY_BAR) {
    drawBar(rPOWER.getRecentBatteryFraction(), true, false);
  }
}

static void drawRssiBar() {
  if (DRAW_RSSI_BAR) {
    float rssiFraction = rBT.getRssiFraction();
    if (rssiFraction < 0.0) {
      drawBar(1.0, false, true);
    } else {
      drawBar(rssiFraction, false, false);
    }
  }
}

void Display::smudge() {
  needsRefresh = true;
}

static void turnOn() {
  if (!isCurrentlyOn) {
    M5.Axp.SetLDO2(true);
    isCurrentlyOn = true;
  }
}

static void maybeTurnOff() {
  if (isCurrentlyOn) {
    bool isPlugged = rPOWER.isPluggedIn();
    //Serial.printf("DISP checking plugged in %s\n", isPlugged?"Y":"N");
    if (isPlugged  &&  DISPLAY_ALWAYS_ON_WHILE_PLUGGED) {
      return;
    }
    if (!isPlugged  &&  DISPLAY_ALWAYS_ON_WHILE_UNPLUGGED) {
      return;
    }
    M5.Axp.SetLDO2(false);
    isCurrentlyOn = false;
  }
}

/**
 * Draws a single icon. There's a lot of tedious arithmetic in here.
 * Icon is always centered along the X and Y dimension. Depending on
 * the physical screen dimensions, there may be padding in addition
 * to the icon itself.
 *
 * Returns the new value for the next Y position.
 */
static int drawTargetIconAt(Target *t, int y, bool hasFocus) {
  const unsigned char *iconBits = smallIcons.at(t->getType());
  int iconSize =  SMALL_ICON_SIZE;
  int iconPad2X = SMALL_ICON_PAD_2X;
  int iconPadX =  SMALL_ICON_PAD_2X / 2;
  int iconPad2Y = SMALL_ICON_PAD_2Y;
  int iconPadY =  SMALL_ICON_PAD_2Y / 2;
  int disabledHalfThickness = 3; // slash drawn 2x this value, plus 1
  if (hasFocus) {
    iconBits = largeIcons.at(t->getType());
    iconSize =  LARGE_ICON_SIZE;
    iconPad2X = LARGE_ICON_PAD_2X;
    iconPadX =  LARGE_ICON_PAD_2X / 2;
    iconPad2Y = LARGE_ICON_PAD_2Y;
    iconPadY =  LARGE_ICON_PAD_2Y / 2;
    disabledHalfThickness = 5;
  }
  int posX = iconPadX;
  int posY = iconPadY + y;
  int nextY =  y + iconSize + iconPad2Y;

  //Serial.printf("drawing icon %s at x=%d y=%d yin=%d, padx=%d, pady=%d\n", t->name, posX, posY, y, iconPadX, iconPadY);

  uint16_t iconColor = COLOR_FOR_NEUTRAL;
  if (t->getState() == Enabled)  iconColor = COLOR_FOR_ENABLED;
  else if (t->getState() == Disabled) iconColor = COLOR_FOR_DISABLED;
  uint16_t bgColor = COLOR_FOR_BACKGROUND;

  if (FLIP_ICON_COLOR) {
    uint16_t temp = bgColor;
    bgColor = iconColor;
    iconColor = temp;
    M5.Lcd.fillRect(0,y, DISPLAY_SHORT_DIMENSION, nextY, bgColor);
  }

  M5.Lcd.fillRect   (posX,posY,           iconSize,iconSize, bgColor);
  M5.Lcd.drawXBitmap(posX,posY, iconBits, iconSize,iconSize, iconColor);
  if (hasFocus) {
    uint16_t boxColor = COLOR_FOR_FOCUS_BOX;
    if (USE_ICON_COLOR_FOR_FOCUS_BOX_COLOR) {
      boxColor = iconColor;
    }
    for (int rr=0; rr<3; ++rr) {
      M5.Lcd.drawRect(posX+1+rr,posY+1+rr, iconSize-2-2*rr,iconSize-2-2*rr, boxColor);
    }
  }
  if (t->getState() == Disabled) {
    int upperRightX = posX + iconSize - 2;
    int lowerLeftX  = posX + 1;
    int upperRightY = posY + 1;
    int lowerLeftY  = posY + iconSize - 2;
    uint16_t slashColor = COLOR_FOR_DISABLED_SLASH;
    if (USE_ICON_COLOR_FOR_DISABLED_SLASH_COLOR) {
      slashColor = iconColor;
    }
    M5.Lcd.drawLine   (upperRightX,upperRightY, lowerLeftX, lowerLeftY, slashColor);
    for (int dd=1; dd<=disabledHalfThickness; ++dd) {
      M5.Lcd.drawLine   (upperRightX-dd, upperRightY,    lowerLeftX,    lowerLeftY-dd, slashColor);
      M5.Lcd.drawLine   (upperRightX,    upperRightY+dd, lowerLeftX+dd, lowerLeftY,    slashColor);
    }
  }

  return nextY;
}

static void drawTargets() {
  if (TARGETS_ORIENTATION == M5ButtonBottom) {
    M5.Lcd.setRotation(0);
  } else {
    M5.Lcd.setRotation(2);
  }
  M5.Lcd.fillScreen(COLOR_FOR_BACKGROUND);
  Target *targetWithFocus = rTARGETSET.getTargetWithFocus();
  int nextY = 0;
  for (int ii=0; ii<rTARGETSET.getNumberOfTargets(); ++ii) {
    Target *t = rTARGETSET.getTarget(ii);
    bool hasFocus = (t == targetWithFocus);
    nextY = drawTargetIconAt(t, nextY, hasFocus);
  }
  drawBatteryBar();
  drawPowerBlotch();
  drawRssiBar();
}

#ifdef M5STICKC_PLUS
// offsets and fonts determined by trial and error, not by clever math
#  define CURSOR_Y_OFFSET 50
#  define CURSOR_X_OFFSET 20
#  define FONT_REMUTER (&FreeSerif18pt7b)
#  define FONT_MCS     (&FreeSerifItalic18pt7b)
#  define FONT_MAC     (&FreeSans12pt7b)
#  define FONT_CONN    (&FreeSerif12pt7b)
#  define FONT_COUNT   (&FreeSans12pt7b)
#else
#  define CURSOR_Y_OFFSET 20
#  define CURSOR_X_OFFSET  0
#  define FONT_REMUTER (&FreeSerif12pt7b)
#  define FONT_MCS     (&FreeSerifItalic12pt7b)
#  define FONT_MAC     (&FreeSans9pt7b)
#  define FONT_CONN    (&FreeSerif9pt7b)
#  define FONT_COUNT   (&FreeSans9pt7b)
#endif

static unsigned long lastPluggedInWithoutBluetooth = 0;
/**
 * The splash screen does double duty. It has the splash screen
 * advertising role, but it's also the "bluetooth connection"
 * status screen when not yet connected.
 */
static void drawSplash() {

  if (SPLASH_ORIENTATION == M5ButtonRight) {
    M5.Lcd.setRotation(1);
  } else {
    M5.Lcd.setRotation(3);
  }
  M5.Lcd.fillScreen(COLOR_FOR_BACKGROUND);

  M5.Lcd.setTextWrap(false);
  M5.Lcd.setTextColor(COLOR_FOR_ENABLED, COLOR_FOR_BACKGROUND);
  M5.Lcd.setCursor(CURSOR_X_OFFSET, CURSOR_Y_OFFSET);
  M5.Lcd.setFreeFont(FONT_REMUTER);
  M5.Lcd.printf("Remuter");
  M5.Lcd.setFreeFont(FONT_MCS);
  M5.Lcd.printf("MCS");

  drawBatteryBar();
  drawPowerBlotch();
  drawRssiBar();
  if (SPLASH_ORIENTATION == M5ButtonRight) {
    M5.Lcd.setRotation(1);
  } else {
    M5.Lcd.setRotation(3);
  }

  M5.Lcd.setFreeFont(FONT_MAC);
  M5.Lcd.printf("\n");
  M5.Lcd.setCursor(CURSOR_X_OFFSET, M5.Lcd.getCursorY());
  M5.Lcd.setTextColor(COLOR_FOR_NEUTRAL, COLOR_FOR_BACKGROUND);
  std::string btMacAddress = rBT.getMacAddress();
  M5.Lcd.printf("%s", btMacAddress.substr(0, 12).c_str());
  M5.Lcd.setTextColor(COLOR_FOR_ENABLED, COLOR_FOR_BACKGROUND);
  M5.Lcd.printf("%s", btMacAddress.substr(12).c_str());
  int secondRowCursorY = M5.Lcd.getCursorY();

  M5.Lcd.setFreeFont(FONT_CONN);
  if (rBT.isConnected()) {
    M5.Lcd.setTextColor(COLOR_FOR_ENABLED, COLOR_FOR_BACKGROUND);
    M5.Lcd.printf("\n");
    M5.Lcd.setCursor(CURSOR_X_OFFSET, M5.Lcd.getCursorY());
    M5.Lcd.printf("BT connected");
  } else {
    long timeRemainingSecs = 9999999;
    if (rPOWER.isPluggedIn()) {
      lastPluggedInWithoutBluetooth = 0;
    } else {
      unsigned long now = millis();
      if (lastPluggedInWithoutBluetooth == 0) {
        lastPluggedInWithoutBluetooth = now;
      }
      unsigned long beenTryingThisManySecs = (now - lastPluggedInWithoutBluetooth) / 1000;
      timeRemainingSecs = UNPLUGGED_BLUETOOTH_CONNECTION_TIMEOUT_SECS - beenTryingThisManySecs;
      Serial.printf("timeRemainingSecs %d\n", timeRemainingSecs);
      if (timeRemainingSecs < 0) {
        Serial.printf("Time out, no Bleutooth while unplugged for %d seconds", UNPLUGGED_BLUETOOTH_CONNECTION_TIMEOUT_SECS);
        M5.Axp.PowerOff();
      }
    }
    M5.Lcd.setTextColor(COLOR_FOR_DISABLED, COLOR_FOR_BACKGROUND);
    M5.Lcd.printf("\n");
    M5.Lcd.setCursor(CURSOR_X_OFFSET, M5.Lcd.getCursorY());
    M5.Lcd.printf("BT");
    char b[] = " connecting ...";
    char *cptr = b;
    char c;
    while ((c = *cptr++) != 0) {
      M5.Lcd.printf("%c", c);
      delay(50);
    }
    if (timeRemainingSecs < 100) {
      M5.Lcd.setFreeFont(FONT_COUNT);
      if (timeRemainingSecs > 30) {
        M5.Lcd.setTextColor(COLOR_FOR_DISABLED, COLOR_FOR_BACKGROUND);
      } else {
	// To draw attention during the final countdown period, put the number in reverse video.
	// I tried just swapping the colors, but it didn't do what I expected. The rectangle
	// gets clipped by the graphics package to the limit of the screen.
	int rectangleHeight = M5.Lcd.getCursorY() - secondRowCursorY;
	M5.Lcd.fillRect(M5.Lcd.getCursorX(), secondRowCursorY+5, DISPLAY_LONG_DIMENSION, rectangleHeight, COLOR_FOR_DISABLED);
        M5.Lcd.setTextColor(COLOR_FOR_BACKGROUND, COLOR_FOR_DISABLED);
      }
      M5.Lcd.printf("%ds", timeRemainingSecs);
    }
  }
}

void Display::manageState(WhatToDisplay whatToDisplay) {
  unsigned long now = millis();
  if (needsRefresh) {
    expiration = max(expiration, now + DISPLAY_TIMEOUT_MILLIS);
    // turn display on and draw
    if (!isCurrentlyOn) {
      //Serial.println("Turning display ON");
      turnOn();
    }

    switch (whatToDisplay) {
      case Targets:
        drawTargets();
        break;
      case Splash:
        drawSplash();
        break;
    }

    needsRefresh = false;
  }

  // This ignores the time spent redrawing the
  // display, but that doesn't matter in human time.
  // Also, the display might turn off immediately at
  // the clock rollover point, but live with it.
  if (isCurrentlyOn  &&  now > expiration) {
    //Serial.println("Turning display OFF");
    maybeTurnOff();
  }
}

Display rDISPLAY;

