/**
 * This simple sketch cycles through the Remuter F-keys.
 * Every time you push the big "M5" button, it sends a
 * key over Bluetooth.
 * 
 * See https://hackaday.io/project/177896-remuter
 * and https://gitlab.com/wjcarpenter/remuter
 */
#include <BleKeyboard.h>

// M5Stick-C
// GPIO 37 is the big button on the same side as the display
const int buttonPin = 37; // the number of the pushbutton pin
const int ledPin =  10;
int buttonState = 0;
int lastButtonState = LOW;

unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 50;

int pressCounter = 0;
BleKeyboard bleKeyboard("Remuter");
// key names are defined in BleKeyboard.h
unsigned char fKeys[] = {KEY_F7, 
                         KEY_F8, 
                         KEY_F9, 
                         KEY_F10, 
                         KEY_F11, 
                         KEY_F12};
int numberOfKeys = sizeof(fKeys) / sizeof(fKeys[0]);

void setup() {
  Serial.begin(115200);
  Serial.println("\nStarting....");
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);

  // set initial LED state HIGH (off)
  digitalWrite(ledPin, HIGH);
  bleKeyboard.begin();
  while (!bleKeyboard.isConnected()) {
    delay(100);
  }
  Serial.println("BT connected");
}

void loop() {
  delay(10); // give it a break
  int reading = digitalRead(buttonPin);
  unsigned long now = millis();
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = now;
  }

  if ((now - lastDebounceTime) > debounceDelay) {
    int ledState = HIGH;
    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // send a key on button pressed down
      // LED is on while button is pressed, off when released
      if (buttonState == LOW) {
        int whichKeyPosition = (pressCounter++) % numberOfKeys;
        unsigned char keyValue = fKeys[whichKeyPosition];
        Serial.print("sending Control Shift F"); 
        Serial.println(keyValue - (KEY_F1 - 1), DEC);
        bleKeyboard.press(KEY_LEFT_CTRL);
        bleKeyboard.press(KEY_LEFT_SHIFT);
        bleKeyboard.press(keyValue);
        bleKeyboard.releaseAll();
        
        ledState = LOW;
      }
      digitalWrite(ledPin, ledState);
    }
  }

  lastButtonState = reading;
}
